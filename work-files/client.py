import socket

SERVER_IP = "127.0.0.1"
SERVER_PORT = 8820


def create_sock():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    return sock


def main():
    sock = create_sock()
    data = "Hello" 

    server_msg = sock.recv(4096)
    server_msg = server_msg.decode()
    print(server_msg)

    if (server_msg == data) :
        sock.sendall(data.encode())
    
    
    
    sock.close()


if __name__ == "__main__":
    main()
