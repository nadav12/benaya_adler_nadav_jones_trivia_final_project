#pragma once
#include "IDatabase.h"
#include "Constants.h"
#include "exceptions.h"

#include <mongocxx/client.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/exception/exception.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/pool.hpp>
#include <mongocxx/exception/query_exception.hpp>
#include <mongocxx/instance.hpp>
#include <boost/optional/optional_io.hpp>
#include <string>
#include <cstdint>
#include <iostream>
#include <vector>

#include "json.hpp"
#include "picosha2.h"

using json = nlohmann::json;

class MongoDatabase : public IDatabase
{
public:
	//User management functions
	virtual bool doesUserExist(string username);
	virtual bool checkPassword(string username, string password);
	virtual void signupUser(string email, string username, string password);
	
	//Optional parameter for the maximum amount of highscores to return
	virtual std::vector<highscore> getHighscores(unsigned int num_scores = DEFAULT_HIGHSCORE_LIMIT);
	virtual void registerHighscore(highscore score);


	static MongoDatabase& getInstance();

	//Delete functions in order to prevent copying
	MongoDatabase(const MongoDatabase&) = delete;
	MongoDatabase(MongoDatabase&&) = delete;
	MongoDatabase& operator=(const MongoDatabase&) = delete;
	MongoDatabase& operator=(MongoDatabase&&) = delete;

private:
	MongoDatabase() = default;
	//It's reccommended to give each thread its own mongocxx::client for thread safety
	mongocxx::database& _getDb();
	mongocxx::instance _instance;
	mongocxx::pool _pool{ mongocxx::uri{ MONGO_URI } };

	static auto getDocument(json j) { return bsoncxx::from_json(j.dump()); }
	//Hashes a password n times and returns the hex representation of the hash
	static string getHexPBKDFHash(string password, string salt, string pepper=PEPPER, int iterations=HASH_ITERATIONS);
};

