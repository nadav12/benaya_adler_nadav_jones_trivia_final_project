//#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "server.h"
#include <iostream>
#include <exception>



int main()
{

	try
	{
		WSAInitializer wsaInit;

		Server myServer;
		myServer.run();

	}
	catch (std::exception& e)
	{
		std::cout << "Server failure, error occured: " << e.what() << std::endl;
	}

	return 0;
}
