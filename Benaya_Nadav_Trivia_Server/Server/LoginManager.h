#pragma once
#include "SqliteDataBase.h"
#include "LoggedUser.h"
#include <regex>

#define MINIMUM_PASSWORD_LEN 8


class LoginManager
{

public:
	LoginManager(IDataBase* db);
	~LoginManager();
	void signup(std::string username, std::string password, std::string email, std::string homeAddress, std::string phoneNumber, std::string birthDate);
	void login(std::string username, std::string password);
	void logout(std::string username);



private:

	IDataBase* m_database;
	std::vector<LoggedUser> m_loggedUsers;

	// Bonus REGEX UserData functions

	bool checkValidPassword(std::string password);
	bool checkValidEmail(std::string email);
	bool checkValidAddress(std::string address);
	bool checkValidNumber(std::string phoneNumber);
	bool checkValidBirthDate(std::string birthDate);

};
