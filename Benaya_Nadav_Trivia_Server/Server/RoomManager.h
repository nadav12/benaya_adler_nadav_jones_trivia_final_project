#pragma once
#include "Room.h"
#include "LoggedUser.h"
#include "SqliteDataBase.h"
#include <vector>
#include <mutex>
#include <map>


class RoomManager
{
public:
	RoomManager(IDataBase* db);
	int createRoom(std::string roomName, unsigned int maxUsers, unsigned int questionCount, unsigned int answerTimeout, LoggedUser user);
	void deleteRoom(int ID);
	unsigned int getRoomState(unsigned int ID);
	std::vector<RoomData> getRooms();
	Room& getRoom(int id);
	void removePlayer(LoggedUser user, unsigned int id);

private:
	unsigned int counter = 0;
	static std::map<unsigned int, Room*> m_rooms;
	IDataBase* m_database;
	std::mutex counter_mutex;
};
