#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(Room room, LoggedUser loggedUser, RequestHandlerFactory& hanlderFactory) : m_handlerFactory(hanlderFactory)
{
	m_room = room;
	m_user = loggedUser;
	
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo request)
{
	bool flag = false;
	if (request.id == LEAVE || request.id == STATE || request.id == GET_PLAYER || request.id == GET_ROOM)
	{
		flag = true;
	}
	return flag;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo request)
{
	RequestResult requestRes{ std::vector<unsigned char>(), nullptr };
	GetPlayersInRoomRequest getPlayersReq{ 0 };

	if (isRequestRelevant(request))
	{
		switch (request.id)
		{
		case LEAVE:
			requestRes = leaveRoom(request);
			break;

		case GET_ROOM:
		case GET_PLAYER:
		{
			getPlayersReq = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(request.buffer);
			requestRes = getPlayersInRoom(getPlayersReq);
			break;
		}
		case STATE:
			requestRes = getRoomState(request);
			break;

		default:
			requestRes.response = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "Error with request id" });
			break;
		}
	}
	else
	{
		//If excpetions are thrown about the request type they will be caught here
		requestRes.response = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "Request doesnt Exist" });
	}

	return requestRes;
}

RequestResult RoomMemberRequestHandler::getPlayersInRoom(GetPlayersInRoomRequest getPLayerReq)
{
	RequestResult reqResult;
	GetPlayersInRoomResponse getPlayersInRoomRes{ std::vector<std::string>() };
	LeaveRoomResponse leaveRoomResponse;
	

	try
	{
		this->m_handlerFactory.getRoomManager().getRoom(this->m_room.getRoomData().id);
	}
	catch (std::exception e)
	{
		
		try
		{
			leaveRoomResponse.status = 1;
			reqResult.response = JsonResponsePacketSerializer::serializeResponse(leaveRoomResponse);
			reqResult.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_room, m_user);
		}
		catch (std::exception e)
		{
			reqResult.response = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ e.what() });
		}

		return reqResult;
	}

	try
	{
		//Get all users
		std::vector<LoggedUser> usersInRoom = m_handlerFactory.getRoomManager().getRoom(getPLayerReq.roomId).getAllUsers();
		//Get all usernames
		for (auto vecIter = usersInRoom.begin(); vecIter != usersInRoom.end(); vecIter++)
		{
			getPlayersInRoomRes.players.push_back((*vecIter).getUsername());
		}
		//Serialize answer
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomRes);
		reqResult.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_room, m_user);
	}
	catch (std::exception e)//If parameters failed the error will be serialized instead
	{
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ e.what() });
	}

	return reqResult;
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo request)
{
	RequestResult result;

	try
	{
		LeaveRoomResponse leaveRoomResponse;
		leaveRoomResponse.status = 1;

		try
		{
			this->m_handlerFactory.getRoomManager().removePlayer(m_user, this->m_room.getRoomData().id);
		}
		catch (std::exception e) {}
		
		result.response = JsonResponsePacketSerializer::serializeResponse(leaveRoomResponse);
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);

	}
	catch (std::exception e)//If serialization failed the error will be serialized instead
	{
		result.response = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ e.what() });
	}

	
	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo request)
{
	RequestResult result;

	std::vector<std::string> players;

	//Create vector of usernames
	for (LoggedUser user : m_room.getAllUsers())
	{
		players.push_back(user.getUsername());
	}

	try
	{
		//build response
		GetRoomStateResponse stateRes{ 1, m_room.getRoomData().isActive, players , m_room.getRoomData().numOfQuestions, m_room.getRoomData().timePerQuestion };
		result.response = JsonResponsePacketSerializer::serializeResponse(stateRes);
		result.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_room, m_user);
	}
	catch (std::exception e)//If serialization failed the error will be serialized instead
	{
		result.response = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ e.what() });
	}

	return result;
}
