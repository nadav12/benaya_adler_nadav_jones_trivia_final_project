#include "LoginManager.h"

//C'Tor
LoginManager::LoginManager(IDataBase* db)
{
	m_database = db;
}

//D'Tor
LoginManager::~LoginManager()
{

}


/*FOR ALL FUNCTION BELOW

The function contant the database to insert / get information.
In case of failure or incorrect parametes, exceptions will be thrown

In: parameters neccessery for query.
*/


void LoginManager::signup(std::string username, std::string password, std::string email, std::string homeAddress, std::string phoneNumber, std::string birthDate)
{
	// I know it breaks DRY.. didnt really know how to do it in another way

	if (!checkValidEmail(email))
	{
		throw std::exception("Invalid email");
	}
	if (!checkValidPassword(password))
	{
		throw std::exception("Invalid password");
	}
	/*
	if (!checkValidAddress(homeAddress))
	{
		throw std::exception("Invalid house address");
	}
	if (!checkValidNumber(phoneNumber))
	{
		throw std::exception("Invalid phone number");
	}
	if (!checkValidBirthDate(birthDate))
	{
		throw std::exception("Invalid birthdate");
	}
	*/

	if (m_database->doseUserExist(username))
	{
		throw std::exception("User already exist");
	}

	m_database->addNewUser(username, password, email, homeAddress, phoneNumber, birthDate);
}


void LoginManager::login(std::string username, std::string password)
{
	if (!m_database->doseUserExist(username))
	{
		throw std::exception("Username doesn't exists");
	}

	if (!m_database->doesPasswordMatch(password))
	{
		throw std::exception("Incorrect password or username");
	}

	m_loggedUsers.push_back(LoggedUser(username));
}



void LoginManager::logout(std::string username)
{
	if (!m_database->doseUserExist(username))
	{
		throw std::exception("Username doesn't exists");
	}

	//Find user in vector and delete him.
	for (std::vector<LoggedUser>::iterator vIter = m_loggedUsers.begin(); vIter != m_loggedUsers.end(); vIter)
	{
		if (vIter->getUsername() == username)
		{
			m_loggedUsers.erase(vIter);
			return;
		}
	}

	throw std::exception("User isn't logged");

}


/*FOR ALL FUNCTION BELOW

The function contant the database to insert / get information.
In case of failure or incorrect parametes, exceptions will be thrown

In: parameters neccessery for query.
*/

bool LoginManager::checkValidPassword(std::string password)
{
	const std::regex pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^&*]).+$");

	if (!password.empty() && password.length() >= MINIMUM_PASSWORD_LEN && std::regex_match(password, pattern))
	{
		return true;
	}
	else
	{
		return false;
	}

}

bool LoginManager::checkValidEmail(std::string email)
{
	const std::regex pattern("^(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+$");

	if (!email.empty() && std::regex_match(email, pattern))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool LoginManager::checkValidAddress(std::string address)
{
	const std::regex pattern("^([a-zA-Z]), (\\d+), ([a-zA-Z])$");

	if (!address.empty() && std::regex_match(address, pattern))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool LoginManager::checkValidNumber(std::string phoneNumber)
{
	const std::regex pattern("^0\\d{1,2}");

	if (!phoneNumber.empty() && std::regex_match(phoneNumber, pattern))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool LoginManager::checkValidBirthDate(std::string birthDate)
{
	const std::regex pattern("^\\d{4}[./]\\d{2}[./]\\d{2}$");

	if (!birthDate.empty() && std::regex_match(birthDate, pattern))
	{
		return true;
	}
	else
	{
		return false;
	}

}
