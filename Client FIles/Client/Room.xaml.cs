﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;


namespace Client
{
    /// <summary>
    /// Interaction logic for Room.xaml
    /// </summary>
    public partial class Room : Window
    {
        private BackgroundWorker refreshThread;


        public Room(RoomData data, bool isAdmin)
        {
            InitializeComponent();
            this.data = data;
            this.isAdmin = isAdmin;
            this.refreshMu = new Mutex(false, "room_refresh_mutex");
            room_name.Text = "ROOM NAME: " + data.name;

            if (!isAdmin)
            {
                start_button.Visibility = Visibility.Hidden;
                exit_room.Content = "exit room";
            }

            // Start refresh thread
            refreshThread = new BackgroundWorker();
            refreshThread.WorkerSupportsCancellation = true;
            refreshThread.DoWork += Automatic_Refresh;
            refreshThread.RunWorkerAsync();

            //If user is admin he was already added when creating the room
            if (!isAdmin) { addUserToRoom(); }
            Show();
     
        }

        public void Refresh()
        {
            byte[] request;
            byte[] result;

            request = Serializer.SerializeRequest(new GetPlayersInRoomRequest(data.id));

            Helper.communicatorMutex.WaitOne();
            Communicator.SendMessage(request);
            //Recieve message
            byte[] serverResponse = Communicator.recieveMessage();
            Helper.communicatorMutex.ReleaseMutex();

            result = Helper.DisassembleResponse(serverResponse);

            //Deserialize response according to CODE (first byte)
            //Console.WriteLine((int)serverResponse[0]);

            if ((int)serverResponse[0] == (int)CODES.ERROR)
            {
                ErrorResponse errRes = Deserializer.DeserializeErrorResponse(result);
                this.Dispatcher.Invoke(() =>
                {
                    Show();
                });
            }
            else if ((int)serverResponse[0] == (int)CODES.GET_PLAYER)
            {
                GetPlayersInRoomResponse playersRes = Deserializer.DeserializeGetPlayersResponse(result);
                Queue<string> players = playersRes.players;

                while (true)
                {
                    if (refreshMu.WaitOne())
                    {
                        try
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                players_list.Items.Clear();

                                
                                admin.Text = "ADMIN: " + players.Peek();
                                foreach (var player in players)
                                {
                                    players_list.Items.Add(player);
                                }
                            });
                            
                        }
                        finally
                        {
                            refreshMu.ReleaseMutex();
                        }
                        break;
                    }
                    else
                    {
                        Thread.Sleep(100);
                        continue;
                    }
                }
                
                /*
                this.Dispatcher.Invoke(() =>
                {
                    Show();
                });
                */

            }
            else if(((int)serverResponse[0] == (int)CODES.LEAVE))
            {
                request = Serializer.SerializeLeaveRoomRequest();
                this.Dispatcher.Invoke(() =>
                {
                    refreshThread.CancelAsync();
                    leaveCloseRoom(request);
                    Close();
                });
                
            }

        }

        private void leaveCloseRoom(byte[] request)
        {
            Helper.communicatorMutex.WaitOne();//Socket mutex on
            Communicator.SendMessage(request);
            byte[] serializedResponse = Communicator.recieveMessage();
            Helper.communicatorMutex.ReleaseMutex();//Socket mutex off

            //Get the response itself (without code and length)
            byte[] result = Helper.DisassembleResponse(serializedResponse);
            if (serializedResponse[0] == (int)CODES.ERROR)
            {
                ErrorResponse errRes = Deserializer.DeserializeErrorResponse(result);
                Error error = new Error();
                error.updateMessage(errRes.message);
                error.Show();
            }
            else
            {
                Menu menu = new Menu();
                menu.Show();
            }

        }

        private void Automatic_Refresh(object sender, DoWorkEventArgs e)
        {
            while (refreshThread.CancellationPending == false)
            {
                Refresh();
                //Wait 3 seconds
                Thread.Sleep(3000);
            }
            e.Cancel = true;
        }

        private void exit_room_Click(object sender, RoutedEventArgs e)
        {
            //In version v3.0.0 send leave room request for user
            //and delete room request for admin
            byte[] request = GetRequest(isAdmin);
            leaveCloseRoom(request);
            refreshThread.CancelAsync();
            Close();
        }
        private byte[] GetRequest(bool isAdmin)
        {
            byte[] request;
            if (isAdmin)
            {
                request = Serializer.SerializeCloseRoomRequest();
            }
            else
            {
                request = Serializer.SerializeLeaveRoomRequest();
            }
            return request;
        }

        /// <summary>
        /// This function adds the current user to the room
        /// </summary>
        public void addUserToRoom()
        {
            byte[] request = Serializer.SerializeRequest(new JoinRoomRequest(data.id));
            Helper.communicatorMutex.WaitOne();
            Communicator.SendMessage(request);
            byte[] serializedResponse = Communicator.recieveMessage();
            Helper.communicatorMutex.ReleaseMutex();
            byte[] result = Helper.DisassembleResponse(serializedResponse);

            //Deserialize response according to CODE (first byte)
            if ((int)serializedResponse[0] == (int)CODES.ERROR)
            {
                ErrorResponse errRes = Deserializer.DeserializeErrorResponse(result);
                Error error = new Error();
                error.updateMessage(errRes.message);
                error.Show();
            }
            else if ((int)serializedResponse[0] == (int)CODES.JOIN)
            {
                JoinRoomResponse joinRes = Deserializer.DeserializeJoinRoomResponse(result);
            }
        }


        private void start_Click(object sender, RoutedEventArgs e)
        {
            // V4 sucks
        }

        private Mutex refreshMu;
        private RoomData data;
        public bool isAdmin { set; get; }
        
    }
}
