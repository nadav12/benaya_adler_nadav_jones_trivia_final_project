﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;


namespace Client
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Window
    {
        private Mutex refreshMu;
        private List<Button> buttons;
        private BackgroundWorker refreshThread;

        public JoinRoom()
        {
            InitializeComponent();

            buttons = new List<Button>();
            this.refreshMu = new Mutex(false, "join_room_refresh_mutex");
            refreshThread = new BackgroundWorker();
            refreshThread.WorkerSupportsCancellation = true;
            refreshThread.DoWork += Automatic_Refresh;
            refreshThread.RunWorkerAsync();

            //ic.ItemsSource = buttons;

            GetRoomsResponse getRoomsRes = Helper.GetRooms();
            if (getRoomsRes != null)
            {
                //Add button for each room
                Button temp;
                foreach (RoomData room in getRoomsRes.rooms)//Iterate over all rooms
                {
                    temp = new Button { Content = room };
                    temp.Click += (sender, e) =>
                    {
                        JoinRoom_Click(sender, e);
                    };
                    buttons.Add(temp);
                }
            }
            else
            {
                Close();
            }

            ic.ItemsSource = buttons;
        }

        private void refresh()
        {
            GetRoomsResponse getRoomsRes = Helper.GetRooms();
            if (getRoomsRes != null)
            {
                //Add button for each room
                Button temp;
                foreach (RoomData room in getRoomsRes.rooms)//Iterate over all rooms
                {
                    while(true)
                    {
                        if (refreshMu.WaitOne())
                        {
                            try
                            {
                                this.Dispatcher.Invoke(() =>
                                {
                                    buttons.Clear();

                                    temp = new Button { Content = room };
                                    temp.Click += (sender, e) =>
                                    {
                                        JoinRoom_Click(sender, e);
                                    };
                                    buttons.Add(temp);
                                });
                            }
                            finally
                            {
                                refreshMu.ReleaseMutex();
                            }
                            break;

                        }
                        else
                        {
                            Thread.Sleep(100);
                            continue;
                        }
                    }

                }
            }
            else
            {
                Close();
            }
            
        }

        private void Automatic_Refresh(object sender, DoWorkEventArgs e)
        {
            while (refreshThread.CancellationPending == false)
            {
                refresh();
                //Wait 3 seconds
                Thread.Sleep(3000);
            }
            e.Cancel = true;
        }

        public void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            refreshThread.CancelAsync();
            Button button = (Button)sender;
            Room room = new Room((RoomData)button.Content,false);
            room.addUserToRoom();
            room.Show();
            Close();           
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {   
            refreshThread.CancelAsync();
            Menu menu = new Menu();
            menu.Show();
            Close();
        }
    }
}

