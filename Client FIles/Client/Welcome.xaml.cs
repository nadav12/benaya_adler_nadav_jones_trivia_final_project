﻿using Microsoft.Win32;
using System;
using System.Media;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for Welcome.xaml
    /// </summary>
    public partial class Welcome
    {
        SoundPlayer player = new SoundPlayer("Client\\indian.wav");
        Random selectRand = new Random();
        const string adDir = "Ads";

        public Welcome()
        {
            InitializeComponent();

            //player.Load();
            //player.PlayLooping();

            Image[] ads = { ad1, ad2, ad3, ad4, ad5, ad6 };
            string[] imagePaths = System.IO.Directory.GetFiles(System.IO.Path.GetFullPath(adDir));

            for (int i = 0; i < ads.Length; i++)
            {
                BitmapImage bitmapImage = new BitmapImage(new Uri(imagePaths[selectRand.Next(imagePaths.Length)]));
                ads[i].Source = bitmapImage;
            }

        }

        private void Register(object sender, RoutedEventArgs e)
        {
            Registration register = new Registration();
            register.Show();
            Close();
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.Show();
            Close();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

       
        private void Ad_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=xvFZjo5PgG0&ab_channel=DeeckPeeck");
        }
        
    }
}
