import socket
import sys
import json

TYPE_INDEX = 0
SIZE_OF_DATA_TYPE = 1
SIZE_OF_DATA_LENGTH = 4
START_LENGTH_INDEX = 1
END_LENGTH_INDEX = 5
MSG_SIZE = 2048
SERVER_PORT = 8820
SERVER_IP = "127.0.0.1"
LOGIN = 100 # cood for login
SIGNUP = 101 # cood for signup


def decoder(sock, msg):
    """
    the function decode the binary messeges into dictionary based on the protocol
    :param sock: the socket of the connection, socket
    :param msg: the msg to decode, string
    :return: dictionary that holds the decoded data, dict
    """
    new_msg = {} #declear
    #decode the type of the msg
    new_msg["type"] = str(msg[TYPE_INDEX])
    #decode the len of the msg
    len = int.from_bytes(msg[START_LENGTH_INDEX: END_LENGTH_INDEX], byteorder='big')
    #recive the remaining data and create keys in the dict
    msg = sock.recv(len)
    new_msg["len"] = len
    new_msg["data"] = str(int.from_bytes(len, byteorder='little'))
    return new_msg

def serialize_request(data, type):

    # convert it to bytes
    packet = type.to_bytes(SIZE_OF_DATA_TYPE, byteorder='little')
    packet += len(json.dumps(data)).to_bytes(SIZE_OF_DATA_LENGTH, byteorder='big')
    packet += json.dumps(data).encode('utf-8')

    return packet

def build_msg(type_msg):
    """
    the function builed msg in the protocol format
    :param type_msg: the type of the msg (login || sign up), int
    :return: the builded packet, int
    """
    data = {} #declear

    if type_msg == LOGIN:
        #create keys with values
        data["username"] = input("enter your username: ")
        data["password"] = input("enter your password: ")

        return serialize_request(data, LOGIN)

    elif type_msg == SIGNUP:
        # create keys with values
        data["username"] = input("enter your username: ")
        data["password"] = input("enter your password: ")
        data["mail"] = input("enter your email: ")

        return serialize_request(data, SIGNUP)

def run_choice(sock, choice, type):

    msg = build_msg(type)
    send_msg(sock, msg)
    return choice


def send_request(sock):
    """
    the function send the request based on user desition
    :param sock: the socket, socket
    :return: the request, string
    """
    while True:
        #get the wanted request from the user
        choice = input("enter request, login / sign up / exit\n")

        #based on user desition, builed the msg and send it
        if choice == "login":
            run_choice(sock, choice, LOGIN)
            break

        elif choice == "sign up":
            run_choice(sock, choice, SIGNUP)
            break

        #if the user asked for exit, print msg to the user and return the choice.
        elif choice == "exit":
            print("exit accomplished\n")
            return choice
            break
        else:
            print("try again, bad input")


def hello_handler(sock):

    # using function to get the answer of the server and print it
    server_msg = sock.recv(SIZE_OF_DATA_TYPE + SIZE_OF_DATA_LENGTH).decode()
    print(server_msg)
    client_msg = "hello"

    # send greeting
    try:
        sock.sendall(client_msg.encode())
    except Exception as e:
        print("ERORR:", e)
        sock.close()
        sys.exit()




def get_server_msg(sock):
    """
    the function gets a socket and get from the server its messege
    :param: sock: a socket that the user is useing, socket
    :return: the messege from the server, string
    """

    # get message from socket, if it failed close socket and program
    try:
        msg = sock.recv(SIZE_OF_DATA_TYPE + SIZE_OF_DATA_LENGTH)
        msg = decoder(sock, msg)
        return msg

    except Exception as e:
        print("ERORR:", e)
        sock.close()
        sys.exit()



def send_msg(sock, request):
    """
    the function send msg to the server
    :param sock: the socket, socket
    :param request: the request to send to the server, int
    :return: NONE
    """
    try:
        sock.sendall(request)
        print("the request is: " + request.decode())
    except Exception as e:
        # if it failed close the socket and the program
        print("ERORR:", e)
        sock.close()
        sys.exit()


def main():
    # create socket with the server
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    # try to connect, if it failed print the error and close the program
    try:
        sock.connect(server_address)
    except Exception as e:
        print("ERROR:", e)
        sock.close()
        sys.exit()

    hello_handler(sock)
    while True:
        if send_request(sock) == "exit":
            break
        else:
            print("the answer is: " + get_server_msg(sock))

    # close the socket
    sock.close()


if __name__ == '__main__':
    main()
